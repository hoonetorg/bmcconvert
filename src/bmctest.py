#!/usr/bin/env python3
import argparse
import sys
import logging
import os
import time

import pprint

logging.basicConfig(level=logging.NOTSET,
                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s '
                    '- %(funcName)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

LOG = logging.getLogger()

try:
        import pywsman
except ImportError:
        pass

BMC_AWAKE_INTERVAL = 60
BMC_AWAKE_CACHE = {}

def awake_bmc(bmc_address):
    """Wake up BMC with ping
    """

    now = int(time.time())
    last_awake = BMC_AWAKE_CACHE.get(bmc_address, 0)
    diff_awake = now - last_awake

    LOG.debug('\n    bmc_address(type: {}): {}\n'
            '    now(type: {}) {}\n'
            '    last_awake(type: {}) {}\n'
            '    diff_awake(type: {}) {}\n'
            '    BMC_AWAKE_INTERVAL(type: {}) {}\n'
            '    BMC_AWAKE_CACHE(type: {})\n'
            '        {}\n\n'.format(
                    type(bmc_address), bmc_address,
                    type(now), now,
                    type(last_awake), last_awake,
                    type(diff_awake), diff_awake,
                    type(BMC_AWAKE_INTERVAL), BMC_AWAKE_INTERVAL,
                    type(BMC_AWAKE_CACHE), pprint.pformat(BMC_AWAKE_CACHE)))

    if diff_awake > BMC_AWAKE_INTERVAL:
        try:
            os.system('ping -i 0.2 -c 5 {}'.format(bmc_address))
        except Exception as e:
            LOG.exception('Unable to awake BMC interface on bmc_address '
                      '{}. Error: {}'.format(
                      bmc_address, e))
            raise Exception
        else:
            LOG.debug('Successfully awakened BMC interface on bmc_address '
                       '{}.'.format(bmc_address))
            BMC_AWAKE_CACHE[bmc_address] = now

def get_out_power_state(out_address, out_username, out_password):
    client = pywsman.Client(
            out_address, 
            16992, 
            '/wsman', 
            'http', 
            out_username, 
            out_password)
    options = pywsman.ClientOptions()
    doc = client.get(options, 
            'http://schemas.dmtf.org/wbem/wscim/1/cim-schema'
            '/2/CIM_AssociatedPowerManagementService')

    LOG.debug('\n'
            '\noptions(type: {})\n'
            '    {}\n'
            'doc(type: {})\n'
            '    {}\n'.format(
                type(options), pprint.pformat(options),
                type(doc), doc))

def main():
    parser = argparse.ArgumentParser(
        prog='bmctest',
        description='Test target BMCs',
    )

    parser.add_argument('--out-address',
                        dest='out_address',
                        help=('The address of the out BMC'))
    parser.add_argument('--out-username',
                        dest='out_username',
                        default='admin',
                        help=('The username for the out BMC '
                              'connection; defaults to "admin"'))
    parser.add_argument('--out-password',
                        dest='out_password',
                        help=('The password for the out BMC '
                              'connection'))

    args = parser.parse_args()
    
    awake_bmc(bmc_address = args.out_address)

    get_out_power_state(out_address  = args.out_address,
            out_username = args.out_username,
            out_password = args.out_password)

if __name__ == '__main__':
    sys.exit(main())
